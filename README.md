# thoth-cli

## Installation

Run the following command on any Inria machine (your workstation or edgar etc...) to install the thoth-cli.

You can also install it on your personal computer to enable certain commands that will automatically be run over SSH on your workstation.
```bash
curl -sSL https://gitlab.inria.fr/thoth-tools/thoth-cli/-/raw/main/lib/install.sh | bash
```

## Usage

This is a command line tool to interact with thoth resources from your workstation or a personal computer (currently support only Linux and MacOS).

It adds the following commands to your system:
- `thoth`: the main command to manage thoth CLI (configuration, update, etc...)
- `monika`: a command to use monika from everywhere. It allow you to get info in the terminal or to open a web browser to see the monika webpage (it will create a proxy if you're not inside the VPN).
- `oarssh`: same a oarsub but creates a SSH tunnel once the job is running.
- `oarlog`: show the output of a job that is running on the cluster.
- `thoth-grafana`: open the grafana of the thoth cluster in your web browser (it will create a proxy if you're not inside the VPN).

For all these commands you can use the `help` option to get more information about the available subcommands and options. For example `thoth help` will give you the list of available subcommands for the `thoth` command.