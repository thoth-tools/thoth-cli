#!/bin/bash

export THOTH_HOME=$HOME/.thoth

# add thoth to the path
export PATH=$THOTH_HOME/cli/bin:$PATH

# load config if it exists
if [ -f "$THOTH_HOME/config" ]; then
  set -a
  source "$THOTH_HOME/config"
  set +a
fi

. "$THOTH_HOME/cli/lib/utils.sh"
if is_on_cluster_node; then
  . "$THOTH_HOME/cli/lib/oar.sh"
  ensure_cluster_env
fi

# make sure oar commands are available anywhere
. "$THOTH_HOME/cli/lib/oarcommands.sh"
