#!/bin/bash

[[ -z "$THOTH_HOME" ]] && {
  echo "THOTH environment isn't activated"
  exit 1
}
. "$THOTH_HOME/cli/lib/utils.sh"

thoth_config_changed=0

thoth_prompt_setting() {
  setting_name=$1
  setting_prompt=$2
  # if not set, prompt until a value is entered
  default_value="${3-}"
  # if default value is set, display it
  if [ -n "$default_value" ]; then
    setting_prompt="$setting_prompt [$default_value]"
  fi
  setting_prompt="$setting_prompt: "
  # prompt for value (if nothing provided and no default value is set, prompt until a value is entered)
  while [ 1 ]; do
    read -rp "$setting_prompt" "${setting_name?}"
    if [ -n "${!setting_name}" ]; then
      thoth_config_changed=1
      break
    elif [ -n "$default_value" ]; then
      eval "$setting_name=$default_value"
      thoth_config_changed=1
      break
    else
      error "Please enter a value"
    fi
  done
}

thoth_ensure_setting() {
  local setting_name=$1
  local setting_prompt=$2
  local setting_value=${!setting_name-}
  local default_value=${3-}
  if [ -z "${setting_value}" ]; then
    if [ -n "$default_value" ]; then
      thoth_prompt_setting "$setting_name" "$setting_prompt" "$default_value"
    else
      thoth_prompt_setting "$setting_name" "$setting_prompt"
    fi
  fi
}

THOTH_CONFIG="$THOTH_HOME/config"
thoth_save_config() {
  echo "THOTH_WORKSTATION=$THOTH_WORKSTATION" >"$THOTH_CONFIG"
  echo "THOTH_SSH_PORT=$THOTH_SSH_PORT" >>"$THOTH_CONFIG"
}

thoth_config_help() {
  cat <<EOF
THOTH Configuration
  THOTH_WORKSTATION   the hostname of your workstation (apophis, curan, etc...)
  THOTH_SSH_PORT      the port used for SSH tunnels (you must be the only one using this port)
EOF
}

thoth_config_dump() {
  # log level (if not set use echo)
  local output=echo
  case $1 in
  "debug") output=debug ;;
  "info") output=info ;;
  *) fail: "Unexpected level: '$1'" ;;
  esac
  $output "THOTH_WORKSTATION=$THOTH_WORKSTATION"
  $output "THOTH_SSH_PORT=$THOTH_SSH_PORT"
}

thoth_configure() {
  prompt_cmd=${1:-thoth_prompt_setting}
  "$prompt_cmd" THOTH_WORKSTATION "Enter the name of the workstation (apophis, curan, etc...)"
  "$prompt_cmd" THOTH_SSH_PORT "Enter the SSH port to use for SSH tunnels" "22$((UID % 1000))"
  if [[ $thoth_config_changed -eq 1 ]]; then
    thoth_save_config
  fi
}

thoth_ensure_config() {
  thoth_configure thoth_ensure_setting
}
