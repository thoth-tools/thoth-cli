#!/bin/bash

[[ -z "$THOTH_HOME" ]] && {
  echo "THOTH environment isn't activated"
  exit 1
}
. "$THOTH_HOME/cli/lib/utils.sh"

if is_on_cluster_node; then
  . "$THOTH_HOME/cli/lib/oar.sh"
  ensure_cluster_env
fi
