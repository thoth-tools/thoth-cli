#!/bin/bash

# this script is intended to be run on a OAR node to keep the node alive
# it can also advertise it's job ID in a dedicated file

[[ -z "$THOTH_HOME" ]] && {
  echo "THOTH environment isn't activated"
  exit 1
}
. "$THOTH_HOME/cli/lib/utils.sh"

info "launching idle script"

# stay idle
tail -f /dev/null
