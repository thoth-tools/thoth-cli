#!/bin/bash

set -ue -o pipefail

THOTH_HOME=$HOME/.thoth
CLI=$THOTH_HOME/cli

CLI_GIT_URL=https://gitlab.inria.fr/thoth-tools/thoth-cli.git

# check if THOTH_HOME exists
if [ ! -d "$THOTH_HOME" ]; then
  echo "Creating $THOTH_HOME"
  mkdir "$THOTH_HOME"
fi

# check if CLI exists
if [ ! -d "$CLI" ]; then
  echo installing cli
  git clone $CLI_GIT_URL "$CLI"
else
  # check if repository is dirty
  if [ -n "$(git -C $CLI status --porcelain)" ]; then
    echo "CLI Repository ($CLI) is dirty. Skipping update."
  else
    echo updating cli
    (
      cd "$CLI"
      git pull
      cd -
    )
  fi
fi

# activate THOTH environment in current shell
. "$THOTH_HOME/cli/lib/activate.sh"
. "$THOTH_HOME/cli/lib/utils.sh"

# activate THOTH environment in shell profile
update_profile() {
  thoth_initialize_block() {
    # add a new line before just in case the files doesn't end with one
    echo ""
    echo "# >>> THOTH initialize >>>"
    echo "#!! Contents within this block are managed by 'thoth install' !!"
    echo "THOTH_HOME=\$HOME/.thoth"
    echo "if [ -d \"\$THOTH_HOME\" ]; then"
    echo "  . \"\$THOTH_HOME/cli/lib/activate.sh\""
    echo "fi"
    echo "# <<< THOTH initialize <<<"
  }
  profile_file=$1
  if [ -f "$profile_file" ]; then
    # FIXME: ugly as shit
    if [ -z "$(grep '# >>> THOTH initialize >>>' "$profile_file")" ]; then
      debug "Adding THOTH initialization block to $profile_file"
      thoth_initialize_block >>"$profile_file"
    else
      # if the bloack already exists, update it
      debug "Updating THOTH initialization block in $profile_file"
      sed --in-place=".bak" '/# >>> THOTH initialize >>>/,/# <<< THOTH initialize <<</d' "$profile_file"
      thoth_initialize_block >>"$profile_file"
    fi
  else
    fail "Profile file $profile_file does not exist"
  fi
}

alert_profile() {
  rc_file=$1
  echo
  warn "To make sure the THOTH environment is activated on interactive and non-interactive login shells"
  warn "make sure the following lines (or similar) are in your .profile:"
  echo "if [ -f \$HOME/$rc_file ]; then"
  echo "  . \$HOME/$rc_file"
  echo "fi"
  echo
}

# update profile depending on the shell
case "$SHELL" in
/bin/bash)
  update_profile "$HOME/.bashrc"
  alert_profile .bashrc
  ;;
/bin/zsh)
  update_profile "$HOME/.zshrc"
  alert_profile .zshrc
  ;;
*)
  fail "Unsupported shell: $SHELL"
  ;;
esac

. "$THOTH_HOME/cli/lib/config.sh"
thoth_ensure_config
