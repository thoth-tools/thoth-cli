#!/bin/bash

[[ -z "$THOTH_HOME" ]] && {
  echo "THOTH environment isn't activated"
  exit 1
}
. "$THOTH_HOME/cli/lib/utils.sh"

oar_env_file_path() {
  # load OAR environment
  # get filename in /var/lib/oar that contains only numbers
  env_file=$(ls /var/lib/oar | grep -E "^$(whoami)_[0-9]+\.env\$" | head -n 1)
  # if env_file is non empty
  if [ -n "$env_file" ]; then
    env_file_path="/var/lib/oar/$env_file"
    if [ -f "$env_file_path" ]; then
      echo "$env_file_path"
    fi
  fi
}

ensure_oar_env() {
  # check id OAR_JOB_ID is set
  if [ -z "$OAR_JOB_ID" ]; then
    env_file_path="$(oar_env_file_path)"
    if [ -f "$env_file_path" ]; then
      # load the environment
      source "$env_file_path"
    fi
  fi
}

clean_gpu() {
  sudo -E /usr/local/bin/clean_gpu.sh
}

ensure_cuda_visible_devices() {
  if [ -z "$CUDA_VISIBLE_DEVICES" ]; then
    # gpu_setVisibleDevices.sh is super slow so we just use the part we need without
    # the call to `sudo -E /usr/local/bin/clean_gpu.sh` which takes about 6 seconds
    # if command -v gpu_setVisibleDevices.sh >/dev/null 2>&1
    # then
      # source gpu_setVisibleDevices.sh
    # fi
    GPU_IDS=`cat $OAR_RESOURCE_PROPERTIES_FILE | awk -F 'gpuid' '{ print $2 }' | cut -d"'" -f 2`
    TRUE_GPU_IDS="$GPU_IDS"
    GPU_IDS=`echo $GPU_IDS | tr ' ' ','`
    export CUDA_VISIBLE_DEVICES=$GPU_IDS
    # just in case we wan't to call clean_gpu after that we'll need the folowwing env var
    export TRUE_GPU_IDS
	  export OAR_JOBID
  fi
}

ensure_cluster_env() {
  ensure_oar_env
  ensure_cuda_visible_devices
}
