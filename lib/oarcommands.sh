[[ -z "$THOTH_HOME" ]] && {
  echo "THOTH environment isn't activated"
  exit 1
}
. "$THOTH_HOME/cli/lib/ssh.sh"

ensure_oar_command() {
  cmd=$1
  if ! command -v "$cmd" >/dev/null; then
    eval "$(
      cat <<EOF
$cmd() {
  run_on_edgar $cmd "\$@"
}
EOF
    )"
  fi
}

ensure_oar_command oarsub
ensure_oar_command oarstat
ensure_oar_command oardel
