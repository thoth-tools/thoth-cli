#!/bin/bash

[[ -z "$THOTH_HOME" ]] && {
  echo "THOTH environment isn't activated"
  exit 1
}
. "$THOTH_HOME/cli/lib/utils.sh"

THOTH_SSH_DIR=$THOTH_HOME/ssh
THOTH_SSH_KEY=$THOTH_SSH_DIR/id_ed25519

thoth_ensure_ssh_key() {
  if [ ! -d "$THOTH_SSH_DIR" ]; then
    debug "Creating $THOTH_SSH_DIR"
    mkdir "$THOTH_SSH_DIR"
  fi

  if [ ! -f "$THOTH_SSH_KEY" ]; then
    debug "Generating SSH key"
    ssh-keygen -t ed25519 -f "$THOTH_SSH_KEY" -N "" >/dev/null
    # add key to authorized_keys
    # FIXME: allowing access to all inria devices with a passwordless key is a security risk
    cat "$THOTH_SSH_KEY.pub" >>"$HOME/.ssh/authorized_keys"
  fi
}

thoth_cluster_node_ssh_start() {
  # ensure SSH key
  thoth_ensure_ssh_key
  # start SSH server
  debug "Starting SSH server on node (port $THOTH_SSH_PORT)"
  /usr/sbin/sshd -p "$THOTH_SSH_PORT" -f /dev/null -h "$THOTH_SSH_KEY" -o X11Forwarding=yes
  # close on exit
  trap thoth_cluster_node_ssh_stop EXIT
  # share the port to workstation
  debug "Forwarding SSH port to $THOTH_WORKSTATION:$THOTH_SSH_PORT"
  info "You will be able to connect to the node via SSH. Press Ctrl+C to close the connection."
  # auto accept new host keys
  ssh -oStrictHostKeyChecking=accept-new -i "$THOTH_SSH_KEY" -NR "$THOTH_SSH_PORT:localhost:$THOTH_SSH_PORT" "$THOTH_WORKSTATION"
}

thoth_cluster_node_ssh_stop() {
  # stop SSH server
  debug "Stopping SSH server on node (port $THOTH_SSH_PORT)"
  pkill -f "sshd -p $THOTH_SSH_PORT"
}

run_on_edgar() {
  if [[ "$(hostname)" != "edgar" ]]; then
    ssh edgar "$(printf ' %q' "$@")"
  else
    "$@"
  fi
}

thoth_cluster_ensure_no_ssh_job() {
  local oarstat_out
  debug "Checking for running SSH jobs"
  oarstat_out=$(run_on_edgar oarstat -u)
  # check if there are lines containing N=ssh-[numbers]
  local lines
  lines="$(echo "$oarstat_out" | grep 'N=ssh-[0-9]\+' || true)"
  # for each line, get first col (job id) and kill the job
  while read -r line; do
    local job_id
    job_id=$(echo "$line" | cut -d' ' -f1)
    if [ -z "$job_id" ]; then
      continue
    fi
    debug "SSH job found with ID $job_id, killing it"
    run_on_edgar oardel "$job_id"
  done <<<"$lines"
}

thoth_cluster_start_ssh_job() {
  thoth_cluster_ensure_no_ssh_job
  OARSSH_DIR="$THOTH_HOME/.oarssh"
  # patch OARSSH_DIR if not on thoth device
  if ! is_on_thoth_device; then
    OARSSH_DIR="$(ssh "$THOTH_WORKSTATION" echo "\$THOTH_HOME")/.oarssh"
    debug "got OARSSH_DIR=$OARSSH_DIR from workstation"
    debug "ensuring $OARSSH_DIR exists on thoth filesystem"
    ssh "$THOTH_WORKSTATION" mkdir -p "$OARSSH_DIR"
  else
    mkdir -p "$OARSSH_DIR"
  fi

  JOB_NAME="ssh-$(date +%s)"
  debug "launching oarssh with job name $JOB_NAME"
  # run over ssh if not on edgar
  ORARSUB_OUT=$(run_on_edgar oarsub -n "$JOB_NAME" -d "$OARSSH_DIR" "$@" "\$THOTH_HOME/cli/lib/start-ssh-server-on-node.sh" | tee /dev/tty)

  # extrat OAR_JOB_ID from oarsub output
  # here we don't use grep -oP because it's not available on all systems
  # JOB_ID=$(echo "$ORARSUB_OUT" | sed -n 's/^OAR_JOB_ID=\([0-9]\+\)$/\1/p')
  JOB_ID=$(echo "$ORARSUB_OUT" | grep -o 'OAR_JOB_ID=[0-9]\+' | cut -d'=' -f2)

  # on_exit() {
  #   debug "killing job $JOB_NAME with ID $JOB_ID"
  #   run_on_edgar oardel "$JOB_ID"
  # }
  # trap on_exit EXIT

  debug "Job $JOB_NAME is running with ID $JOB_ID"
}
