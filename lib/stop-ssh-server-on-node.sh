#!/bin/bash
set -ue -o pipefail
[[ -z "$THOTH_HOME" ]] && {
  echo "THOTH environment isn't activated"
  exit 1
}
. "$THOTH_HOME/cli/lib/utils.sh"

# this script must only run on a GPU cluster node
if ! is_on_cluster_node; then
  fail "This script must only run on a GPU cluster node"
fi
. "$THOTH_HOME/cli/lib/ssh.sh"
thoth_cluster_node_ssh_stop
