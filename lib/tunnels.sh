#!/bin/bash

[[ -z "$THOTH_HOME" ]] && {
  echo "THOTH environment isn't activated"
  exit 1
}
. "$THOTH_HOME/cli/lib/utils.sh"

open_browser() {
  local url="$1"

  if [[ -n "${BROWSER-}" ]]; then
    $BROWSER "$url"
  else
    if [[ $(uname) == "Darwin" ]]; then
      open "$url"
    else
      xdg-open "$url"
    fi
  fi
}

close_tunnel() {
  local local_port="$1"
  local pid
  pid="$(lsof -ti :$local_port)"
  debug "Closing tunnel on port $local_port with pid $pid"
  if [[ -n "$pid" ]]; then
    kill "$pid"
  fi
}

tunnel_website() {
  local name="$1"
  local url="$2"
  # get adress from url
  local host
  host="$(echo "$url" | sed -E 's/http:\/\/([^\/]+).*/\1/')"
  port=80
  # set port if in url
  if [[ "$host" == *":"* ]]; then
    port="$(echo "$host" | sed -E 's/.*:(.*)/\1/')"
    host="$(echo "$host" | sed -E 's/(.*):.*/\1/')"
  fi
  local path
  path="$(echo "$url" | sed -E 's/http:\/\/[^\/]+(.*)/\1/')"
  # check if website is reachable (timeout 1s)
  local reachable
  reachable="$(curl --connect-timeout 1 -Is "$url" >/dev/null && echo "true" || echo "false")"
  debug "Checking if $name is reachable: '$reachable'"
  if [ "$reachable" = "false" ]; then
    debug "$name is not directly reachable, creating a tunnel"
    local local_port=8074
    # find free port
    while :; do
      nc -z localhost "$local_port" 2>/dev/null || break
      debug "Port $local_port is not available"
      local_port=$((local_port + 1))
    done
    debug "Port $local_port is available"
    # forward port 80 of edgar to local port using workstation
    ssh -N -L "$local_port:$host:$port" "$THOTH_WORKSTATION" &
    # wait for the tunnel to be established
    sleep 3
    # close the tunnel on exit
    trap "close_tunnel $local_port" EXIT
    # update url
    url="http://localhost:$local_port$path"
  fi
  info "$name is available at $url"
  debug "Opening $name in browser"
  open_browser "$url"
  wait
}
