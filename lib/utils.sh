#!/bin/bash

info() {
  echo -e "\033[0;32m$1\033[0m"
}

debug() {
  echo -e "\033[0;36m$1\033[0m"
}

warn() {
  echo -e "\033[0;33m$1\033[0m"
}

error() {
  echo -e "\033[0;31m$1\033[0m"
}

fail() {
  error "$1"
  exit 1
}

is_on_thoth_device() {
  # FIXME: might not be the best way
  [ -d /scratch/clear/thoth ]
}

is_on_cluster_node() {
  [[ $(hostname) == gpuhost* ]]
}
